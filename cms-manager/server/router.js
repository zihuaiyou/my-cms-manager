const express = require('express');
const router = express.Router();
const OptPool = require('./OptPool');
const optPool = new OptPool();
const pool = optPool.getPool();

//登录信息的接口
router.post('/login', (req, res) => {
    //引入的body-parser中间件生成req.body并解析
    const { username, password } = req.body;
    console.log(username, password);
    if (username && password) {
        //执行SQL语句 
        //从连接池中获取一个连接
        pool.getConnection(function (err, conn) {
            //查询 
            conn.query(`SELECT * from user where username = '${username}' and password='${password}'`, function (err, rs) {
                if (err) {
                    console.log('[query] - :' + err);
                    return;
                }
                if (rs.length > 0) {
                    res.send({
                        status: 200,
                        msg: "登录成功",
                    })
                } else {
                    res.send({
                        status: 400,
                        msg: "用户名或密码错误"
                    })
                }
                conn.release(); //放回连接池
            });
        });

    } else {
        res.send({
            status: 400,
            msg: "用户名或密码错误"
        })
    }
})

//注册信息的接口
router.post('/register', (req, res) => {
    //引入的body-parser中间件生成req.body并解析
    const { username, password } = req.body;
    if (username && password) {
        //执行SQL语句 
        //从连接池中获取一个连接
        pool.getConnection(function (err, conn) {
            //先查询有没有该用户 
            conn.query(`SELECT * from user where username = '${username}'`, function (err, rs) {
                if (rs.length > 0) {
                    res.send({
                        status: 200,
                        msg: "该用户已注册！",
                    })
                } else { //insert语句
                    conn.query(`insert into user (username,password) values ('${username}','${password}')`, function (err, rs) {
                        if (rs) {
                            res.send({
                                status: 200,
                                msg: "注册成功！",
                            })
                        }
                    })
                }
                conn.release(); //放回连接池
            });
        });

    } else {
        res.send({
            status: 400,
            msg: "用户名或密码错误"
        })
    }
})

module.exports = router;