import App from '../App.jsx'
import Edit from '../pages/Edit'
import List from '../pages/List'
import Login from '../pages/Login'
import Register from '../pages/Register'
import Means from '../pages/Means'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

const BaseRouter = () => (
    <BrowserRouter>
        <Routes>
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
            <Route path='/' element={<App />} >
                <Route path='/edit' element={<Edit />} />
                <Route path='/list' element={<List />} />
                <Route path='/means' element={<Means />} />
            </Route>
        </Routes>
    </BrowserRouter>
)

export default BaseRouter