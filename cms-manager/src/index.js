import ReactDOM from 'react-dom'
import './assets/base.css'
import BaseRouter from './router'
ReactDOM.render(
    <BaseRouter/>,
    document.getElementById('root')
)