import React from 'react'
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input } from 'antd';
import './styles/login.less'
import Logo from '../assets/logo.png'
import { Link } from 'react-router-dom';
import api from '../api';

export default function Register() {
  const onFinish = (values) => {
    // console.log('Received values of form: ', values);
    const { username, password } = values
    api.postRegisterData({ username, password }).then((res) => {
        console.log(res);
    })
  }
  return (
    <div className='login'>
      <div className='login_box'>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item>
            <img src={Logo} alt="" />
          </Form.Item>
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: '请输入用户名!',
              },
            ]}
          >
            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="用户名" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: '请输入密码!',
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="密码"
            />
          </Form.Item>
          <Form.Item
            name="confirm"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: '请输入密码!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('两次输入密码不一致，请检查!'));
                },
              }),
            ]}
          >
            <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder="确认密码"/>
          </Form.Item>
          <Form.Item>
            <span>
              <Link to="/login">已有账号？立即登录</Link>
            </span>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button" block>
              注册
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>

  )
}
