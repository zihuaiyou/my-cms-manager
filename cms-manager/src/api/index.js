import axios from '../utils/request'
const base = {
    baseUrl: "http://localhost:5566",
    login: "/api/login",
    register: "/api/register",
}
const api = {
    //提交登录信息
    postLoginData(params) {
        return axios.post(base.baseUrl + base.login, params)
    },
    //注册用户
    postRegisterData(params) {
        return axios.post(base.baseUrl + base.register, params)
    },
}

export default api;